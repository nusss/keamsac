// the number of oscillators in the oscillator bank
k = 10;

// define the SynthDef
(
SynthDef(\oscBank, {
	// the variable 'bank' holds n simple sinus oscillators, by default
	// automatically multichannel-expanded to n channels
	// 'stereo' spreads the output of 'bank' accross the stereo field
	var bank = SinOsc.ar(\freq.kr(200!k/*, 0.3*/)),
		stereo = Splay.ar(bank, spread: 0.5, level: 0.2, center: 0);
	// route 'stereo' to bus 0
	// 2 channels (as derived from 'stereo')
	Out.ar(0, stereo);
}).add;
)

// instantiate a new Synth from the previously defined SynthDef
a = Synth(\oscBank);
// set random frequencies in the oscillator bank
a.setn(\freq, Array.fill(k, { 100.rrand(4000) }));

// the IP address on which VideOSC is running
n = NetAddr("192.168.1.3", 32000);

// set the frequency of each oscillator within the oscillator bank in Synth a
// from within an OSCdef, each listening to a different messages coming one pixel
// the following 'do' loop automatically creates k different OSCdefs
// and provides a counter variable i
(
k.do{ |i|
	OSCdef(\freq ++ i, { |msg|
		var val = #[100, 4000, \exp].asSpec.map(msg[1]);

		// set oscillator at index i in Synth a
		a.seti(\freq, i, val);
		// send feedback to VideOSC
		// displayed within the pixel that,
		// sent the incoming value
		n.sendMsg('/vosc/blue' ++ (i+1), "freq["++i++"]");
	},
	// OSC command pattern, pixels start at 1, not 0: /vosc/blue1 to /vosc/blueN
	'/vosc/blue' ++ (i+1))
}
)

// clean up
OSCdef.freeAll;
a.free;
n.disconnect;